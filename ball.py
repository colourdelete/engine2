from direct.actor.Actor import Actor
from panda3d.bullet import BulletTriangleMesh, BulletTriangleMeshShape, BulletWorld, BulletRigidBodyNode

from assets import Assets


class Ball(Actor):
    def __init__(
            self,
            assets: Assets,
    ):
        Actor.__init__(
            self,
            assets['ball']['path'],
            {},
        )
        self.physics_shape_mesh = BulletTriangleMesh()
        self.physics_shape_mesh.add_geom(
            self.getGeomNode().findAllMatches('**/+GeomNode').getPath(0).node().getGeom(0),
            # get all nodes of the actor and use it as the physics mesh thing
        )
        self.physics_shape = BulletTriangleMeshShape(self.physics_shape_mesh, dynamic=True)
        self.physics_node = BulletRigidBodyNode('Ball')
        self.physics_node.add_shape(self.physics_shape)
        self.physics_node.set_mass(1.0)
        self.np = None
        self.orig_pos = (0, 0, 0)

    def apply(self, base, world: BulletWorld):
        self.np = base.render.attach_new_node(self.physics_node)
        self.reparent_to(self.np)
        base.task_mgr.add(self.__reset)
        world.attach_rigid_body(self.physics_node)

    def __reset(self, task):
        limit = 10
        if abs(self.np.get_x()-self.orig_pos[0]) > limit or abs(self.np.get_y()-self.orig_pos[1]) > limit or abs(self.np.get_z()-self.orig_pos[2]) > limit:
            self.physics_node.set_angular_velocity((0, 0, 0))
            self.physics_node.set_linear_velocity((0, 0, 0))
            self.np.set_pos(self.orig_pos)
        return task.cont

    def rm(self):
        self.cleanup()
