from typing import Callable, List, Any

from direct.showbase.ShowBase import ShowBase as original_ShowBase


class ShowBase(original_ShowBase):
    def __init__(self, *args, plugins: List[Callable] = None, **kwargs):
        super().__init__(self, *args, **kwargs)
        if plugins is None:
            plugins = []
        self.__plugins = plugins

    @property
    def plugins(self):
        return self.__plugins

    @plugins.setter
    def plugins(self, plugins: List[Callable]):
        self.__plugins = plugins
        self.init_plugins()

    def init_plugins(self):
        for plugin in self.plugins:
            plugin(self)

    def task(self, *args, **kwargs):
        def decorator(f: Callable) -> Callable:
            self.task_mgr.add(f, *args, **kwargs)
            return f

        return decorator

    def on(self, event: str, method: Callable, extra_args: List[Any] = None):
        if extra_args is None:
            extra_args = []

        def decorator(f: Callable) -> Callable:
            self.accept(event, method, extraArgs=extra_args)
            return f

        return decorator
