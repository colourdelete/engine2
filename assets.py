import json
import os
from pathlib import Path
from typing import Dict

import toml


class Assets(dict):
    def __init__(self, data: Dict[str, object]):
        super(Assets, self).__init__(data)

    @staticmethod
    def from_path(path: str):
        ext = os.path.splitext(path)[1]
        if ext == '.toml':
            with open(path) as file:
                return Assets(toml.load(file))
        elif ext == '.json':
            with open(path) as file:
                return Assets(json.load(file))
        RuntimeWarning(f'unknown file path extension {ext}, returning Assets with blank data')
        return Assets({})
